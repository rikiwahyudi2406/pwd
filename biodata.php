<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 1</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Galeri.html">Galeri</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Berita.html">Berita</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <img src="Image.jpg" class="img-fluid" alt="RIKI WAHYUDI">
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body bg-danger">
                    <table class="table table-hover table-primary">
                        <tbody>
                            <center><h2 style="font-style: oblique;">Biodata Diri</h2></center>
                            <table class="table table-hover table-primary">
                            <tr>
                                <td>NPM</td>
                                <td>:</td>
                                <td>02122001</td>
                            </tr>
                            <tr>
                                <td>NAMA</td>
                                <td>:</td>
                                <Td>RIKI WAHYUDI</Td>
                            </tr>
                            <tr>
                                <td>JENIS KELAMIN</td>
                                <td>:</td>
                                <td>LAKI LAKI</td>
                            </tr>
                            <tr>
                                <td>AGAMA</td>
                                <td>:</td>
                                <td>ISLAM</td>
                            </tr>
                            <tr>
                                <td>TEMPAT ,TANGGAL LAHIR</td>
                                <td>:</td>
                                <td>JAMBI, 24 JUNI 2003</td>
                            </tr>
                            <tr>
                                <td>ALAMAT TEMPAT TINGGAL</td>
                                <td>:</td>
                                <td>KOMPLEK TAMAN ORGAN PERMAI,GENTING HIJAU BLOK E4 NO 16</td>
                            </tr>
                            <tr>
                                <td>NO HP</td>
                                <td>:</td>
                                <td>083843572704</td>
                            </tr>
                            <tr>
                                <td>HOBI</td>
                                <td>:</td>
                                <td>
                                    <ul>
                                    <li>BERMAIN FUTSAL</li>
                                    <li>RIDDING MOTOR</li>
                                    <li>MODIFIKASI MOTOR</li>
                                </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>RIWAYAT PENDIDIKAN</td>
                                <td>:</td>
                                <td>
                                    <ol>
                                    <li>SDN NEGRI 3 KOTA LUBUK LINGGAU</li>
                                    <li>SMP NEGRI 5 KOTA LUBUK LINGGAU</li>
                                    <li>SMK MUHAMADIYAH KOTA LUBUK LINGGAU</li>
                                    <li>SMA 1 MADANG SUKU,KABUPATEN OGAN KOMERING ULU TIMUR</li>
                                    <li>SMA NEGRI 8 KOTA LUBUK LINGGAU</li>
                                    <li>SMA NUSA KOTA LUBUK LINGGAU</li>
                                </ol>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>    
    </div>
    <script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap